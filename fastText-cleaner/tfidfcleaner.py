# -*- coding: utf-8 -*-
#
# 对语料库使用Tf-IDF进行筛选，需要先使用idfcount统计idf值
#
# 输入: map<word, idf>
# 输出: fasttext格式
#
# For Test:
# python3 dataprocess/tfidfrank.py -i ~/szty/train.ft -w wordlist.utf8

from __future__ import division

import argparse
import os
from tqdm import tqdm
import math
import pickle

FILE_ENCODE = 'UTF-8'

def get_args_parser():
    parser = argparse.ArgumentParser(description='tfidf count tool')
    parser.add_argument('-i', '--input',  default='/home/xiabo/szty/train.ft', type=str, help='input file')
    parser.add_argument('-o', '--output', default='train.st.tfidf.seged', type=str, help='output file')
    parser.add_argument('-w', '--wordidf', default='/home/xiabo/szty/train+addon+valid+2017.idf.pkl', type=str, help='wordlist')
    parser.add_argument('-l', '--labelidf', default='/home/xiabo/PatClassify/train.tfidfclass.pkl', type=str, help='wordlist')
    parser.add_argument('--maxlabelidf', default=13, type=float, help='max labelidf filter threshold')
    parser.add_argument('--minlabelidf', default=0.003, type=float, help='min labelidf filter threshold')
    parser.add_argument('-n', '--maxcount', default=300, type=int, help='maxcount')
    parser.add_argument('-r', '--replicate', default=20, type=int, help='replicate')
    parser.add_argument('-t', '--titleiter', default=5, type=int, help='titleiter')
    return parser.parse_args()

def glance(a):
    b = 5
    for c in a:
        if b > 0:
            print(c + ":\t" + str(a[c]))
        b-=1

if __name__ == '__main__':
    args = get_args_parser()
    print(args)

    with open(args.wordidf, 'rb') as fin:
        WORDIDF = pickle.load(fin)
    print(len(WORDIDF))
    glance(WORDIDF)

    # with open(args.labelidf, 'rb') as fin:
    #     LABELIDF = pickle.load(fin)
    # print(len(LABELIDF))
    # glance(LABELIDF)

    print('TF-IDF ranking ' + args.input + "\n")
    FILESIZE = os.path.getsize(args.input)
    FIN      = open(args.input,  'r', encoding=FILE_ENCODE, errors="ignore")
    FOUT     = open(args.output, 'w', encoding=FILE_ENCODE)
    LINENO   = 0
    with tqdm(total=FILESIZE) as pbar:
        LINESIZE = 0
        line = FIN.readline()
        while line:
            LINESIZE = len(line.encode(FILE_ENCODE))
            items = line.strip().split(" ")
            FOUT.write(items[0] + " ")

            words = {}
            nwords = 0
            for item in items[1:]:
                # if item in LABELIDF and ( LABELIDF[item] < args.minlabelidf or LABELIDF[item] > args.maxlabelidf ):
                #     continue

                if item in WORDIDF:
                    nwords+=1
                    if item in words:
                        words[item] += 1
                    else:
                        words[item] = 1
            
            for item in words:
                words[item] = WORDIDF[item] * words[item]
            
            words = sorted(words.items(), key=lambda item:item[1], reverse=True)
            maxcount = args.maxcount if len(words) > args.maxcount else len(words)
            wordset = set()
            for i in range(maxcount):
                wordset.add(words[i][0])

            for _ in range(args.titleiter):
                if len(items)>args.replicate:
                    for item in items[1:args.replicate]:
                        if item in wordset:
                            FOUT.write(item + " ")

            for item in items[1:]:
                if item in wordset:
                    FOUT.write(item + " ")

            FOUT.write("\n")
            line = FIN.readline()
            LINENO += 1
            pbar.update(LINESIZE)
    FIN.close()
    FOUT.close()