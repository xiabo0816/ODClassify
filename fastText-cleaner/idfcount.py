# -*- coding: utf-8 -*-
#
# 对语料库进行ITf-IDF统计
#
# 输入: fasttext格式
# 输出: map<word, idf>
#
# For Test:
# python3 dataprocess/idfcount.py -i ~/szty/train.ft -w wordlist.utf8

from __future__ import division

import argparse
import os
from tqdm import tqdm
import math
import pickle

FILE_ENCODE = 'UTF-8'

def get_args_parser():
    parser = argparse.ArgumentParser(description='tfidf count tool')
    parser.add_argument('-i', '--input',  default='train.seged', type=str, help='input file')
    parser.add_argument('-o', '--output', default='train.tfidf.pkl', type=str, help='input file')
    parser.add_argument('-w', '--wordlist', default='wordlist.utf8', type=str, help='wordlist')
    return parser.parse_args()

def readWord(path, words):
    print('Reading ' + path + "\n")
    COUNT  = 0
    FILESIZE = os.path.getsize(path)
    FIN      = open(path, 'r', encoding=FILE_ENCODE)
    with tqdm(total=FILESIZE) as pbar:
        LINESIZE = 0
        line = FIN.readline()
        while line:
            LINESIZE = len(line.encode(FILE_ENCODE))
            line = line.strip()
            items = line.split("\t")
            
            if len(items) == 0:
                continue
            
            if len(items) == 1:
                words[items[0]] = 0
            if len(items) > 1:
                words[items[0]] = items[1]

            line = FIN.readline()
            pbar.update(LINESIZE)
    FIN.close()
    return COUNT

def IDFCount(path, words, wordidf):
    print('IDFCount ' + path + "\n")
    FILESIZE = os.path.getsize(path)
    FIN      = open(path, 'r', encoding=FILE_ENCODE)
    LINENO   = 0
    with tqdm(total=FILESIZE) as pbar:
        LINESIZE = 0
        line = FIN.readline()
        while line:
            LINESIZE = len(line.encode(FILE_ENCODE))
            items = line.strip().split(" ")
            for item in set(items[1:]):
                if item in wordidf:
                    wordidf[item] += 1
            line = FIN.readline()
            LINENO += 1
            pbar.update(LINESIZE)
    FIN.close()
    return LINENO

def IDFCalculate(docs, wordidf):
    with tqdm(total=len(wordidf)) as pbar:
        for item in wordidf:
            wordidf[item] = math.log(docs / (wordidf[item] + 1))
            pbar.update(1)

def glance(a):
    b = 5
    for c in a:
        if b > 0:
            print(c + ":\t" + str(a[c]))
        b-=1

if __name__ == '__main__':
    args = get_args_parser()
    print(args)

    with open(args.wordlist, 'rb') as fin:
        WORDS = pickle.load(fin)
    print(len(WORDS))
    glance(WORDS)

    WORDIDF = {}
    for item in WORDS:
        WORDIDF[item] = 1
        
    D = IDFCount(args.input, WORDS, WORDIDF)
    IDFCalculate(D, WORDIDF)

    glance(WORDIDF)
    with open(args.output, 'wb') as fout:
        pickle.dump(WORDIDF, fout)
