# -*- coding: utf-8 -*-
#
# 对语料库进行ITf-IDF统计
#
# 输入: fasttext格式
# 输出: map<word, idf>
#
# For Test:
# python3 dataprocess/idfcount.py -i ~/szty/train.ft -w wordlist.utf8
import os
import requests
import argparse
import time
import json
import jieba
import re
from tqdm import tqdm

'''
全局变量
'''
FILE_ENCODE = 'UTF-8'
G_PATTERN_ISNUM = re.compile(r'[\d\.]+')
'''
输入路径和参数配置
'''
# inputfile  = 'gw.ft.train.niltext5'
# outputfile = 'gw.niltext.train'
inputfile  = 'gw.ft.valid.niltext5'
flag   = True
MIN_WORDS_PER_TEXT = 5

stopwords = 'stopwords.txt'

fin = open(inputfile)
if flag:
    fout = open(os.path.basename(inputfile)+".lf", 'w')

def labelclean(line):
    return len(line.split(" ")[0].split("\\")) == 1 and line.split(" ")[0].split("\\")[0] != "__label__其他"

lines = fin.readlines()
total = len(lines)
count = 0
with tqdm(total=total) as pbar: 
    for line in lines:
        if labelclean(line.strip()):
            # print(line)
            count += 1
        else:
            if flag:
                fout.write(line)
        pbar.update(1)
fin.close()
if flag:
    fout.close()

print(count, total, str(count/total))

