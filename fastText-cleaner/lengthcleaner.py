# -*- coding: utf-8 -*-

import os
import requests
import argparse
import time
import json
import jieba
import re
from tqdm import tqdm

'''
全局变量
'''
FILE_ENCODE = 'UTF-8'
G_PATTERN_ISNUM = re.compile(r'[\d\.]+')
'''
输入路径和参数配置
'''
# inputfile  = '../preprocess/data/gw.ft.train'
# outputfile = 'gw.niltext.train'
inputfile  = '../preprocess/data/gw.ft.valid'
flag   = True
MIN_WORDS_PER_TEXT = 5

stopwords = 'stopwords.txt'

fin = open(inputfile)
if flag:
    fout = open(os.path.basename(inputfile)+".niltext"+str(MIN_WORDS_PER_TEXT), 'w')

lines = fin.readlines()
total = len(lines)
count = 0
with tqdm(total=total) as pbar: 
    for line in lines:
        words = [word for word in line.strip().split(" ") if not word.startswith("__label__") and word != '']
        if len(words) < MIN_WORDS_PER_TEXT:
            count += 1
            # print(str(len(words)))
            # print(words)
            # print(line)
            # input()
        else:
            if flag:
                fout.write(line)

        # text = [word for word in list(jieba.cut(line['text'])) if (len(G_PATTERN_ISNUM.findall(word)) == 0) and word not in STOPWORDS]
        # text = " ".join(text)
        # fout.write('__label__' + item['label'].split('\\')[0] + ' ' + text + '\n')
        pbar.update(1)
fin.close()
if flag:
    fout.close()

print(count, total, str(count/total))

