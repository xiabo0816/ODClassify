# -*- coding: utf-8 -*-
import fasttext
import pickle
import jieba
import json
import os
import re

CLASSIFY_MODEL  = None
TFIDF_MODEL = None
LABEL_LIST  = None
STOPWORDS   = None
G_PATTERN_ISNUM = re.compile(r'[\d\.]+')
MAX_COUNT = 300
REPLICATE_CHARS = 20
REPLICATE_TIMES = 5
FILE_ENCODE = 'UTF-8'

def init(path):
    global CLASSIFY_MODEL
    global TFIDF_MODEL
    global STOPWORDS
    global LABEL_LIST
    if not os.path.isfile(os.path.join(path, 'stopwords.utf8')):
        raise Exception("'stopwords.utf8' not in " + path)
    if not os.path.isfile(os.path.join(path, 'model.bin')):
        raise Exception("'model.bin' not in " + path)
    if not os.path.isfile(os.path.join(path, 'tfidf.pkl')):
        raise Exception("'tfidf.pkl' not in " + path)
    if not os.path.isfile(os.path.join(path, 'labellist.json')):
        raise Exception("'labellist.json' not in " + path)
    STOPWORDS      = read_word_list(os.path.join(path, 'stopwords.utf8'))
    CLASSIFY_MODEL = fasttext.load_model(os.path.join(path, 'model.bin'))
    
    print('Loading model from:', os.path.join(path, 'model.bin'))
    
    LABEL_LIST     = json.load(open(os.path.join(path, 'labellist.json')))
    with open(os.path.join(path, 'tfidf.pkl'), 'rb') as fin:
        TFIDF_MODEL = pickle.load(fin)

def decode_content(content, topn):
    label, prob = CLASSIFY_MODEL.predict(tfidf(segmenter(content)), k=topn)
    return label, prob

def segmenter(text):
    text = jieba.cut(text.replace("\n", ""))
    text = [word for word in text if (len(G_PATTERN_ISNUM.findall(word)) == 0) and word not in STOPWORDS]
    # text = [word for word in list(jieba.cut(line['text'])) if (len(G_PATTERN_ISNUM.findall(word)) == 0) and word not in STOPWORDS]
    return " ".join(text)

def read_word_list(path):
    if (path == ''):
        return []
    words = [line.strip() for line in open(path, 'r', encoding=FILE_ENCODE).readlines()]
    return words

def tfidf(text):
    items = text.strip().split(" ")
    words = {}
    nwords = 0
    for item in items:
        if item in TFIDF_MODEL:
            nwords+=1
            if item in words:
                words[item] += 1
            else:
                words[item] = 1
    
    for item in words:
        words[item] = TFIDF_MODEL[item] * words[item]
    
    words = sorted(words.items(), key=lambda item:item[1], reverse=True)
    maxcount = MAX_COUNT if len(words) > MAX_COUNT else len(words)
    wordset = set()
    for i in range(maxcount):
        wordset.add(words[i][0])

    result = ''
    for _ in range(REPLICATE_TIMES):
        if len(items)>REPLICATE_CHARS:
            for item in items[:REPLICATE_CHARS]:
                if item in wordset:
                    result += item + " "

    for item in items:
        if item in wordset:
            result += item + " "
    
    return result


def labelwithid(label):
    if label == "其他":
        label = "其他\\其他"
    label1, label2 = label.split("\\")

    value1 = -1
    value2 = -1
    # print(LABEL_LIST)
    for i in LABEL_LIST:
         if i['label'] == label1:
            value1 = i['value']
            for j in i['children']:
                if j['label'] == label2:
                    value2 = j['value']
                    break
            break
    if value1 != -1 and value2 != -1:
        return (label, label1, value1, label2, value2)
    else:
        print('Error: findvalue null')
        return (label, label1, value1, label2, value2)