# Installation

CentOS离线更新到gcc8
```bash
yum check-update
yum install centos-release-scl-rh centos-release-scl
yum install centos-release-scl devtoolset-8-gcc devtoolset-8-gcc-c++ devtoolset-8-binutils
# 在bash下激活，只在当前terminal窗口有效
source /opt/rh/devtoolset-8/enable
```

在CentOS8上安装gcc

```bash
dnf group install "Development Tools"
dnf install man-pages
```

```bash
# 安装依赖包
pip3 install setuptools wheel Cython==3.0a5 cffi greenlet pybind11 SciPy regex gunicorn -i https://mirrors.aliyun.com/pypi/simple/ 
pip3 install flask gevent requests sklearn unrar fasttext jieba torch pytorch_pretrained_bert service_streamer -i https://mirrors.aliyun.com/pypi/simple/ 

# 启动
gunicorn -b 0.0.0.0:10080 -w 4 -k gevent run:app
```

创建虚拟环境
```bash
python3 -m venv gwclavenv/
source gwclavenv/bin/activate.fish
pip3 install --upgrade pip
# pip3 install -i https://mirrors.aliyun.com/pypi/simple/ wheel
# pip3 install -i https://mirrors.aliyun.com/pypi/simple/ flask gevent requests fasttext jieba
```

```bash
curl -X POST http://localhost:39003/content -d '{"content":"汽车", "level":"subclass", "num":2}'
curl -X POST http://localhost:39003/content -d '{"content":"“十四五”时期，是新时代全面推进实现社会主义现代化的开局期，也是加快追赶超越、推动高质量发展的关键期内容。是的撒我们立足实际，在全面总结“十三五”期间经济和社会发展的基础上，深入开展调研，深刻剖析问题，认真研盼国内外形势和发展的机遇与挑战，系统地的谋划思考“十四五”发展目标、战略定位、重大任务，为科学编制“十四五”规划奠定了坚实基础。现将我区“十四五”发思路和重点工作通知如下：", "level":"subclass", "num":2}'
```