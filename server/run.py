# -*- coding: utf-8 -*-
from flask import Flask, request, jsonify
from gevent import monkey; monkey.patch_all()
from time import strftime, localtime
from gevent.pywsgi import WSGIServer
from threading import Timer

import time, sched
import traceback
import requests
import argparse
import model_fasttext
# import model_bert
import logging
import pickle
import socket
import json
import sys
import os

app = Flask(__name__)


g_server_ip = socket.gethostbyname(socket.gethostname())


defaultencoding = 'utf-8'
if sys.getdefaultencoding() != defaultencoding:
    reload(sys)
    sys.setdefaultencoding(defaultencoding)


logger = logging.getLogger(__name__)
logger.setLevel(level = logging.INFO)
handler = logging.FileHandler("requests.log")
handler.setLevel(logging.INFO)
formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s')
handler.setFormatter(formatter)
logger.addHandler(handler)

model_fasttext.init('model/')

def get_args_parser():
    parser = argparse.ArgumentParser(description='公文小类自动分类服务')
    parser.add_argument('-p', '--port',  default=39001, type=int, help='listening port')
    # parser.add_argument('-m', '--model',  default='model/', type=str, help='model path')
    return parser.parse_args()


@app.route("/isactive", methods=["GET"])
def isactive():
    return 'ok'
    

@app.route("/list", methods=["POST"])
def list():
    logger.info(strftime("%Y-%m-%d %H:%M:%S", localtime()))
    try:
        result = {"flag": "True", "code": 200, "msg": "分类成功", "data":model_fasttext.LABEL_LIST}

    except Exception as e:
        print(traceback.format_exc())
        logger.debug(traceback.format_exc())
        logger.debug(e)
        result = {"flag": "False", "code": 500, "msg": traceback.format_exc()}
        return result

    logger.info(result)
    return result


@app.route("/content", methods=["POST"])
def content():
    logger.info(strftime("%Y-%m-%d %H:%M:%S", localtime()))
    try:
        inputs = json.loads(request.get_data())
        print(inputs)
        labels, probs = model_fasttext.decode_content(inputs["content"], inputs["num"])
        result = {"flag": "True", "code": 200, "msg": "分类成功", "serverip": g_server_ip, "data":[]}
        for i in range(len(labels)):
            result["data"].append(model_fasttext.labelwithid(labels[i].replace("__label__", "")))

    except Exception as e:
        print(traceback.format_exc())
        logger.debug(traceback.format_exc())
        logger.debug(e)
        result = {"flag": "False", "code": 500, "msg": traceback.format_exc()}
        return result

    logger.info(inputs)
    logger.info(result)
    print(result)
    return result


if __name__ == "__main__":
    args = get_args_parser()
    print(args)
    logger.info(args)

    print('==> Deploying Official Document Classifier:')
    print('http://localhost:' + str(args.port) + "/content")

    logger.info('==> Deploying Official Document Classifier:')
    logger.info('http://localhost:' + str(args.port) + "/content")

    WSGIServer(("0.0.0.0", args.port), app).serve_forever()
