# 政府公文自动行业分类

[ODClassify(Official documents Classify)](https://gitee.com/xiabo0816/ODClassify)

政府公文是党政机关实施领导、履行职能、处理公务的具有特定效力和规范体式的文书，是传达贯彻党和国家的方针政策，公布法规和规章，指导、布置和商洽工作，请示和答复问题，报告、通报和交流情况等的重要工具。

公文文种有如下15种：决议、决定、命令（令）、公报、公告、通告、意见、通知、通报、报告、请示、批复、议案、函和纪要。

*本分类器是对公文进行`行业分类`，不是文种分类。*


## 行业分类体系

共分为如下22大类，可以在[政府信息公开](http://www.gov.cn/zhengce/xxgk/index.htm)查看到

* 国务院组织机构
* 综合政务
* 国民经济管理、国有资产监管
* 财政、金融、审计
* 国土资源、能源
* 农业、林业、水利
* 工业、交通
* 商贸、海关、旅游
* 市场监管、安全生产监管
* 城乡建设、环境保护
* 科技、教育
* 文化、广电、新闻出版
* 卫生、体育
* 人口与计划生育、妇女儿童工作
* 劳动、人事、监察
* 公安、安全、司法
* 民政、扶贫、救灾
* 民族、宗教
* 对外事务
* 港澳台侨工作
* 国防
* 其他

分类体系继承国务院公文主题分类

# 政府信息公开公文数据获取

线上的页面是这个
http://www.gov.cn/zhengce/xxgk/index.htm

经过页面分析，可以使用get接口直接访问并得到公文链接索引信息
http://xxgk.www.gov.cn/search-zhengce/?page_index=1&page_size=5593

## 下载全部索引url

* **国务院文件**，截止目前共5596篇

```bash
curl "http://xxgk.www.gov.cn/search-zhengce/?page_index=1&page_size=5593" > zhengce.json
```

国务院文件索引结构比较简单，需要的数据直接保存在`['searchVO']['listVO']`字段中

这里需要注意，国务院文件和部门文件的索引文件不同，需要分别做不同的处理

zhengce.json就是最终国务院部门文件的公文链接索引数据

* **国务院部门文件**，截止目前共6967篇

```bash
# 访问的地址
# http://sousuo.gov.cn/data?t=zhengcelibrary_bm&p=0&n=2
# http://sousuo.gov.cn/data?t=zhengcelibrary&q=&timetype=&mintime=&maxtime=&sort=pubtime&sortType=1&searchfield=&pcodeJiguan=&childtype=&subchildtype=&tsbq=&pubtimeyear=&puborg=&pcodeYear=&pcodeNum=&filetype=&p=0&n=5&inpro=&bmfl=&dup=&orpro=
```

该接口pagesize最大为100, 需要循环访问得到全部链接索引

该接口存在屏蔽机制，被屏蔽之后修改链接的pagesize就好，改个大于100的值，因为它最大就是100。「手动捂脸」

该接口获取使用spider/bumen/index.sh文件实现

获取之后，需要做一个预处理：

```python
import json
a = []
for i in range(70):
    b = json.load(open(str(i)+".json"))
    a.extend(b['searchVO']['listVO'])
json.dump(a, open("bumen.json","w"), ensure_ascii=False, indent=4)
```

预处理之后的目录结构。
```bash
└── bumen
    ├── *.json
    ├── bumen.json
    └── index.sh
```

通过index.sh得到多个*.json，通过多个json整合得到最终的bumen.json

bumen.json就是最终国务院部门文件的公文链接索引数据

## 开始运行获取程序

* 安装需要的pip包

```bash
pip3 install -i https://mirrors.aliyun.com/pypi/simple/ beautifulsoup4
```

* 在get公文链接索引信息之后，利用spider/目录下的程序进行获取，
```bash
spider/
├── crawl_bumen.py
└── crawl_zhengce.py
```

在这两个程序中，整体来说这部分的数据清洗工作很重要，还可以仔细分析，来添加更多的判断。

在这两个程序中，分别将数据输出到zhengce.jsons和bumen.jsons数据格式是每行一个json结构，读入时候按行读入即可

每行内包括必要的信息，text和label，数据示例：

```json
{"text": "国家发展改革委商务部关于支持海南自由贸易港建设放宽市场准入若干特别措施的意见", "label": "商贸、海关、旅游\\其他"}
{"text": "国家发展改革委商务部关于支持海南自由贸易港建设放宽市场准入若干特别措施的意见", "label": "商贸、海关、旅游\\其他"}
```

为了方便，可以将这两部分数据合并在一起
```bash
cat zhengce.jsons bumen.jsons > gw.jsons
```

# 提取分类体系

从原始数据利用`preprocess/json2labels.py`可以导出全部的分类体系结构

可以方便快捷的提取分类体系，带层级的json对象数组

```bash
python3 json2labels.py
```

导出全部分类列表，不带层级的纯列表
```python
a = json.load(open('zhengce.json'))
json.dump([i['label'] for i in a], open('zhutitag.list', 'w'), ensure_ascii=False, sort_keys=True, indent=4)
```

# 训练公文分类模型

## 数据预处理

使用`preprocess/json2bert.py`和`preprocess/json2ft.py`可以把上文输出的gw.jsons转换成fastText和其他分类模型需要的数据格式。

有一些分类的标签格式不规范，需要处理：

```
财政、金融、审计 > 其他	1
财政、金融、审计/货币（含外汇）	1
财政、金融、审计/其他	1
科技、教育/科技	1
```

## 利用fastText工具

`fastText`比较轻松的就可以得到一个初步结果，[使用git clone的地址](https://github.com/facebookresearch/fastText/)

为了保证基本效果，`preprocess/json2ft.py`中进行了必要的数据预处理，包括去停用词、去数字、分词

对数据进行打乱和切分
```bash
shuf gw.json > gw.shuf.tmp
wc -l gw.shuf.tmp
head -n xxx gw.shuf.tmp > gw.ft.train
tail -n xxx gw.shuf.tmp > gw.ft.valid

shuf gw.ft.txt > gw.ft.shuf.tmp
head -n 10000 gw.ft.shuf.tmp > gw.ft.train
tail -n 791 gw.ft.shuf.tmp > gw.ft.valid
rm -rf gw.ft.shuf.tmp
```

训练fastText模型，并利用`-autotune-validation`进行自动寻参

```bash
# 训练模型
./fasttext supervised -input ../spider/test.txt -output ../model/test
# 测试模型
./fasttext test ../model/test.bin ../spider/test.txt
```

加载预训练词向量进行训练
1. `cc.zh.300.vec`是fastText团队提供的预训练词向量
2. `Tencent_AILab_ChineseEmbedding.tar.gz`是腾讯发布的预训练词向量

```bash
# 下载预训练词向量
wget https://dl.fbaipublicfiles.com/fasttext/vectors-crawl/cc.zh.300.vec.gz
wget https://ai.tencent.com/ailab/nlp/zh/data/Tencent_AILab_ChineseEmbedding.tar.gz
```

```bash
# 训练模型
./fastText/fasttext supervised -input preprocess/data/zhengce.st.num.train -output model/sub.st+num -wordNgrams 2 -dim 100 -bucket 300000 -lr 1 -autotune-validation ../data/subclass.valid -autotune-duration 600
./fastText/fasttext test model/sub.st.num.bin preprocess/data/zhengce.st.num.valid

./fastText/fasttext supervised -input preprocess/data/gw.train -output model/sub.st.num -wordNgrams 2 -dim 100 -bucket 300000 -lr 1 -autotune-validation ../data/subclass.valid -autotune-duration 600

./fastText/fasttext dump model/sub.st+num.bin args
dim 200
ws 5
epoch 25
minCount 1
neg 5
wordNgrams 2
loss softmax
model sup
bucket 300000
minn 0
maxn 0
lrUpdateRate 100
t 0.0001
# 这个只是打印出来看一下，最理想的参数需要分析和测试

# 测试模型
./fastText/fasttext test model/sub.st+num.bin preprocess/data/gw.valid

# 训练模型
./fasttext supervised -input ../data/subclass.train -output ../model/sub -wordNgrams 2 -dim 300 -loss hs -bucket 300000 -pretrainedVectors ../cc.zh.300.vec
./fasttext supervised -input ../data/subclass.train -output ../model/sub -wordNgrams 2 -dim 100 -loss hs -bucket 300000 -autotune-validation ../data/subclass.valid -autotune-duration 600
# 测试模型
./fasttext test ../model/sub.bin ../data/subclass.valid
```

使用fastText工具，大类可以到70+的准确率、小类可以到60+。

如果还没进入讨论实际使用需求场景的阶段，作为功能demo是可以的

当前效果0.643
```bash
> wc -l fastText-cleaner/gw.ft.train.niltext5.tfidf
9999 fastText-cleaner/gw.ft.train.niltext5.tfidf
> ./fastText/fasttext supervised -input fastText-cleaner/gw.ft.train.niltext5 -output model/04261532-1 -wordNgrams 3 -dim 100 -bucket 300000 -lr 10 -epoch 10 -minCount 1 -ws 10 -lrUpdateRate 10
> ./fastText/fasttext test model/04261532-1.bin fastText-cleaner/gw.ft.valid.niltext5
N       790
P@1     0.643
R@1     0.643


xiabo@bogon ~/g/ODClassify> ./fastText/fasttext supervised -input fastText-cleaner/gw.ft.train.niltext5.lf -output model/04261532-4 -wordNgrams 8 -dim 100 -bucket 300000 -lr 10 -epoch 10 -minCount 1 -ws 10 -minCountLabel 10
Read 4M words
Number of words:  80763
Number of labels: 100
Progress: 100.0% words/sec/thread:   60143 lr:  0.000000 avg.loss:  0.680304 ETA:   0h 0m 0s 
xiabo@bogon ~/g/ODClassify> ./fastText/fasttext test model/04261532-4.bin fastText-cleaner/gw.ft.valid.niltext5
N       780
P@1     0.664
R@1     0.664

./fastText/fasttext supervised -input fastText-cleaner/gw.ft.train.niltext5.tfidf  -output model/04261532-6 -wordNgrams 8 -dim 200 -bucket 300000 -lr 10 -epoch 10 -minCount 3 -ws 10 -minCountLabel 20
./fastText/fasttext test model/04261532-6.bin fastText-cleaner/gw.ft.valid.niltext5.tfidf 
N       768
P@1     0.681
R@1     0.681
```

## 基于BERT模型

待续

BERT应该会准确率高一些

## 部署http分类服务

部署服务程序在`server/`下，使用`flask`和`WSGIServer`进行封装。

1. 安装http分类服务需要的依赖包
```bash
# 建议创建虚拟环境
python3 -m venv gwclavenv/
source gwclavenv/bin/activate.fish
pip3 install --upgrade pip
# pip3 install -i https://mirrors.aliyun.com/pypi/simple/ wheel
# pip3 install -i https://mirrors.aliyun.com/pypi/simple/ flask gevent requests fasttext jieba
```

2. 准备模型数据文件
3. 启动和测试服务
```bash
curl -X POST http://localhost:39003/content -d '{"content":"汽车", "level":"subclass", "num":2}'
curl -X POST http://localhost:39003/content -d '{"content":"“十四五”时期，是新时代全面推进实现社会主义现代化的开局期，也是加快追赶超越、推动高质量发展的关键期内容。是的撒我们立足实际，在全面总结“十三五”期间经济和社会发展的基础上，深入开展调研，深刻剖析问题，认真研盼国内外形势和发展的机遇与挑战，系统地的谋划思考“十四五”发展目标、战略定位、重大任务，为科学编制“十四五”规划奠定了坚实基础。现将我区“十四五”发思路和重点工作通知如下：", "level":"subclass", "num":2}'
```

