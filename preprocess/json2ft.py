# -*- coding: utf-8 -*-

import os
import requests
import argparse
import time
import json
import jieba
import re
from tqdm import tqdm

FILE_ENCODE = 'UTF-8'
G_PATTERN_ISNUM = re.compile(r'[\d\.]+')

input = 'data/gw.jsons'
output = 'data/gw.ft.txt'

stopwords = '../data/stopwords.txt'

def read_word_list(path):
    if (path == ''):
        return []
    words = [line.strip() for line in open(path, 'r', encoding=FILE_ENCODE).readlines()]
    return words

STOPWORDS = read_word_list(stopwords)

def clean_label(label):
    return label.replace(' ', '').replace('/', '\\').replace('>', '\\')

fin = open(input)
fout = open(output, 'w')

lines = fin.readlines()
with tqdm(total=len(lines)) as pbar: 
    for line in lines:
        line = json.loads(line)
        text = [word for word in list(jieba.cut(''.join(line['text'].split()))) if (len(G_PATTERN_ISNUM.findall(word)) == 0) and word not in STOPWORDS]
        if len(line['text']) < 10 or len(line['text']) > 5000:
            pbar.update(1)
            continue
        text = " ".join(text)
        fout.write('__label__' + clean_label(line['label']) + ' ' + text.replace("\r","") + '\n')
        # fout.write('__label__' + item['label'].split('\\')[0] + ' ' + text + '\n')
        pbar.update(1)
fin.close()
fout.close()

