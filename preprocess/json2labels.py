#!/usr/bin/env python
# coding: utf-8
import json

input = 'data/gw.jsons'
output = 'zhutitag.json'

fin = open(input)
data = fin.readlines()
fin.close()

labels = {}
for item in data:
    label = json.loads(item.strip())["label"]
    if len(label.split("\\")) == 1:
        continue
    a, b = label.split("\\")
    if a not in labels:
        labels[a] = []
    if b not in labels[a]:
        labels[a].append(b)
        
labels["其他"] = ["其他"]

count = 0
results = []
for item in labels:
    result = {"children": [], "label":"", "value":0}
    for j in labels[item]:
        result["children"].append({
            "label": j,
            "value": count
        })
        count += 1
    result["value"] = count
    result["label"] = item
    results.append(result)
    count += 1

json.dump(results, open(output, "w"), ensure_ascii=False, indent = 4)
