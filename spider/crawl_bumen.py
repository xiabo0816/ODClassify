# -*- coding: utf-8 -*-

import os
import requests
import argparse
import time
import json
from bs4 import BeautifulSoup
import jieba
from tqdm import tqdm

# g_url = 'https://engine.ipsensus.com/v1/engine/standard'

def get_args_parser():

    parser = argparse.ArgumentParser(description='这个是一个http://www.gov.cn/zhengce/xxgk/index.htm的爬虫')

    parser.add_argument('-i', '--input', default='zhengce.json', type=str, help='input json file')

    parser.add_argument('-o', '--output', default='bumen.jsons', type=str, help='registers')

    return parser.parse_args()


def cleaner(text):
    text = text.replace('\u3000', '')
    text = text.replace('\n\n\n', '').replace('\n\n', '').replace('\n', '').replace("\r","")
    return text
    # return ' '.join(list(jieba.cut(text)))


def htmlparse(html):
    result = {}
    result['text'] = ''
    soup = BeautifulSoup(html, "html.parser")
    for dom in soup.find_all(name='div', attrs={'class':'pages_content'}):
        result['text']+=cleaner(dom.get_text())

    # $(".policyLibraryOverview_header td")[9].innerText
    result['label'] = soup.select('.policyLibraryOverview_header td')[9].get_text()
    return result


def run(targets):
    # headers = {'Content-Type': 'application/json'}
    results = []

    with tqdm(total=len(targets)) as pbar: 
        for target in targets:
            # dime = time.time()
            r = requests.get(target)
            # print('Processing',target,'...')
            # print(r.content.decode('utf-8'))
            tmp = htmlparse(r.content.decode('utf-8'))
            # print(tmp)
            # print("执行时间：", time.time() - dime)
            # input()
            results.append(tmp)
            pbar.update(1)

    return results

def read_list(path):
    a = json.load(open("bumen.json"))
    return [item['url'] for item in a]

if __name__ == '__main__':

    args = get_args_parser()

    print(args)

    targets = read_list(args.input)
    print(len(targets))

    data = run(targets)

    with open(args.output, 'w') as fout:
        for item in data:
            fout.write(json.dumps(item, ensure_ascii=False) + "\n")
            # fout.write('__label__'+item['label'] + ' ' + item['text'] + '\n')

    # with open(args.output + '.section', 'w') as fout:
    #     for item in data:
    #         fout.write('__label__' + item['label'].split('\\')[0] + ' ' + item['text'] + '\n')
